# MC Plugins

## 1st plugin: EatOrDiePlugin
- It is a simple plugin, If you lose food bar. 
- Monster will spawn to kill you.
- Make sure It don't get hungry or they will get you.

---

## Settings:
[Config.yml](./src/main/resources/config.yml)

---

## SpitGot
[EatOrDiePlugin on SpitGot](https://www.spigotmc.org/resources/eat-or-die.78642/)