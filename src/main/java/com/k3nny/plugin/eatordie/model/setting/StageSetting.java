package com.k3nny.plugin.eatordie.model.setting;

import org.bukkit.entity.EntityType;

import com.k3nny.plugin.eatordie.EatOrDiePlugin;

import lombok.Data;

@Data
public class StageSetting {

  private int start = 20;
  private EntityType entityType = EntityType.ZOMBIE;
  private int qty = 1;

  public void setQty(int qty) {
    if (qty < 0) {
      this.qty = 0;
    }
    if (qty > 20) {
      this.qty = 20;
    }
    this.qty = qty;
  }

  public void setStart(int start) {
    if (start < 0) {
      this.start = 1;
    }
    if (start > 20) {
      this.start = 20;
    }
    this.start = start;
  }

  public void setType(String type) {
    if (EntityType.valueOf(type.toUpperCase()) != null) {
      this.entityType = EntityType.valueOf(type);
    } else {
      EatOrDiePlugin.logger()
          .warning("The entity type [" + type + "] doesn't exist. It will spawn 'ZOMBIE'. Fix your config.yml.");
    }
  }
}
