package com.k3nny.plugin.eatordie.listener;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import com.k3nny.plugin.eatordie.EatOrDiePlugin;
import com.k3nny.plugin.eatordie.Setting;
import com.k3nny.plugin.eatordie.model.setting.StageSetting;

public class FoodLevelChangeListener implements Listener {
	
	private Random random;
	
	public FoodLevelChangeListener() {
		
		if (Setting.isEnable()) {
			try {
				random = SecureRandom.getInstanceStrong();
			} catch (NoSuchAlgorithmException e) {
				random = new Random();
			}
		}
	}
	
	private int checkDistanceRange(int value) {
		if (value < -10) {
			return -10;
		}
		if (value > 10) {
			return 10;
		}
		return value;
	}
	
	private int checkFoodLevelRange(int value) {
		if (value < 0) {
			return 0;
		}
		if (value > 20) {
			return 20;
		}
		return value;
	}
	
	private StageSetting factoryStage(Player player) {
		int playerFoodLevel = player.getFoodLevel();
		return Setting.findStageSettingByPlayerFoodLevel(playerFoodLevel);
	}
	
	private int generateRandomValue() {
		int min = checkDistanceRange(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.spawn.distance.min"));
		int max = checkDistanceRange(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.spawn.distance.max"));
		return random.nextInt(max + 1 - min) + min;
	}
	
	private boolean hungerGrowing(int currentFoodLevel, int changeFoodLevel) {
		int stageStart = checkFoodLevelRange(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.stage.start"));
		boolean shouldStart = stageStart >= changeFoodLevel;
		return shouldStart && currentFoodLevel > changeFoodLevel;
	}
	
	private void manageStage(Player player) {
		
		spawnMessage(player);
		
		int randomX = generateRandomValue();
		int randomZ = generateRandomValue();
		
		StageSetting stageSetting = factoryStage(player);
		for (int i = 0; i <= stageSetting.getQty(); i++) {
			player.getWorld().spawnEntity(player.getLocation().add(randomX, 0, randomZ), stageSetting.getEntityType());
		}
		
	}
	
	@EventHandler
	public void onPlayerJoin(FoodLevelChangeEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			
			int currentFoodLevel = player.getFoodLevel();
			int changeFoodLevel = event.getFoodLevel();
			
			if (hungerGrowing(currentFoodLevel, changeFoodLevel)) {
				manageStage(player);
			}
		}
	}
	
	private void spawnMessage(Player player) {
		if (EatOrDiePlugin.getPlugin().getConfig().getBoolean("monster.spawn.alert.enable")) {
			player.sendMessage(EatOrDiePlugin.getPlugin().getConfig().getString("monster.spawn.alert.message"));
		}
	}
}
