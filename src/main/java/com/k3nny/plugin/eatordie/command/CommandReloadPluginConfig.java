package com.k3nny.plugin.eatordie.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.k3nny.plugin.eatordie.EatOrDiePlugin;
import com.k3nny.plugin.eatordie.Setting;

public class CommandReloadPluginConfig implements CommandExecutor {

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

    if (sender.isOp()) {
      EatOrDiePlugin.getPlugin().reloadConfig();
      sender.sendMessage("The Plugin Config has been reloaded.");
      Setting.loadStageSetting();
      sender.sendMessage("The Stage Setting has been loaded.");
    }

    return true;
  }

}
