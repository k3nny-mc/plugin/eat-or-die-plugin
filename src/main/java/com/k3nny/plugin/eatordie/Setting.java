package com.k3nny.plugin.eatordie;

import java.util.ArrayList;
import java.util.List;

import com.k3nny.plugin.eatordie.model.setting.StageSetting;

import lombok.Getter;

@Getter
public class Setting {

	private static boolean enable = true;
  private static List<StageSetting> stages = new ArrayList<>();

  static {
  	enable = EatOrDiePlugin.getPlugin().getConfig().getBoolean("enable");
    loadStageSetting();
  }

  public static StageSetting findStageSettingByPlayerFoodLevel(int playerFoodLevel) {

    StageSetting correctStage = new StageSetting();
    for (StageSetting stage : stages) {
      if (stage.getStart() < correctStage.getStart() && stage.getStart() > playerFoodLevel) {
        correctStage = stage;
      }
      if (stage.getStart() == playerFoodLevel) {
        correctStage = stage;
      }
    }

    return correctStage;
  }
  
  public static boolean isEnable() {
  	return enable;
  }

  public static void loadStageSetting() {
    stages.clear();

    StageSetting stage1 = new StageSetting();
    stage1.setStart(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.stage1.start"));
    stage1.setQty(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.stage1.qty"));
    stage1.setType(EatOrDiePlugin.getPlugin().getConfig().getString("monster.stage1.type"));

    StageSetting stage2 = new StageSetting();
    stage2.setStart(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.stage2.start"));
    stage2.setQty(EatOrDiePlugin.getPlugin().getConfig().getInt("monster.stage2.qty"));
    stage2.setType(EatOrDiePlugin.getPlugin().getConfig().getString("monster.stage2.type"));

    stages.add(stage1);
    stages.add(stage2);

  }

  private Setting() {
    throw new IllegalStateException("Utility class");
  }

}
