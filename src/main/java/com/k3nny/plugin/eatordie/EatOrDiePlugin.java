package com.k3nny.plugin.eatordie;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import com.k3nny.plugin.eatordie.command.CommandReloadPluginConfig;
import com.k3nny.plugin.eatordie.listener.FoodLevelChangeListener;

public class EatOrDiePlugin extends JavaPlugin {

  private static EatOrDiePlugin eatOrDiePlugin;
  private static Logger logger;

  public static EatOrDiePlugin getPlugin() {
    return eatOrDiePlugin;
  }

  public static Logger logger() {
    return logger;
  }

  private void loadComands() {
    this.getCommand("eatordie").setExecutor(new CommandReloadPluginConfig());
  }

  @Override
  public void onDisable() {

  }

  @Override
  public void onEnable() {

    logger = getLogger();

    eatOrDiePlugin = this;

    getConfig().options().copyDefaults(true);
    saveDefaultConfig();

    getServer().getPluginManager().registerEvents(new FoodLevelChangeListener(), this);

    loadComands();

    getLogger().info("Enabled!");

  }
}