# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.3] - 2020-05-17
### Added
 - Add `enable` config. It allow disable the plugin.

### Refactor
 - Improve `pomx.xml` 
 - Remove `lang.` from `config.yml`.

### Fix
 - Fix the external folder to deploy the plugin after `maven install`.

## [1.0.1] - 2020-05-13
### Added
 - Add `Plugins` to `pom.xml` and define `plugin.location`.
 - Add `CHANGELOG.md`
 - Add `distributionManagement` 

### Improved
 - Improve `pom.xml`
 - Improve `.gitignore`
 - Clean up project [code styles]

## [Released]

## [1.0.0] - 2020-05-10
### Added
 - Add `EatOrDiePlugin` 
 - Add `Settings`
 - Add `CommandReloadPluginConfig`
 - Add `FoodLevelChangeListener`
 - Add `StageSetting`
